package com.clientui.clientui;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.clientui.beans.ProductBean;
import com.clientui.proxies.MicroserviceProduitProxy;

@Controller
public class ClientContoller {

	@Autowired
	MicroserviceProduitProxy mProduitProxy;
	
	@RequestMapping("/")
	public String accueil(Model model) {
		
		List<ProductBean> produits =  mProduitProxy.listeDesProduits();
		
		produits.stream().forEach(x -> System.out.println(x));
		
		model.addAttribute("produits", produits);
		return "Accueil";
	}
	
	
	  @RequestMapping("/details-produit/{id}")
	   public String ficheProduit(@PathVariable int id,  Model model){

	       ProductBean produit = mProduitProxy.recupererUnProduit(id) ;

	       model.addAttribute("produit", produit);

	       return "FicheProduit";
	   }
	
	
	
	
}
