package com.clientui.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.clientui.exception.CustomErrorDecoder;

@Configuration
public class FeignExceptionConfiguration {

	@Bean
	public CustomErrorDecoder mCustomErrorDecoder() {
		return new CustomErrorDecoder();
	}
	
}
