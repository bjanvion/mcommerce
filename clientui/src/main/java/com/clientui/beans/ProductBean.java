package com.clientui.beans;

public class ProductBean {

	private int id;

	private String titre;

	private String description;

	private String image;

	private Double prix;

	public ProductBean() {
				
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductBean [id=");
		builder.append(id);
		builder.append(", titre=");
		builder.append(titre);
		builder.append(", description=");
		builder.append(description);
		builder.append(", image=");
		builder.append(image);
		builder.append(", prix=");
		builder.append(prix);
		builder.append("]");
		return builder.toString();
	}
	
	
	// proxy : elemenet pour generer la requete http
	
	

}
