package com.mcommerce.commande.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mcommerce.commande.model.Commande;

@Repository
public interface CommandesDao extends JpaRepository<Commande, Integer> {
}
