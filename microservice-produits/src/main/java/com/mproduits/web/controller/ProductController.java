package com.mproduits.web.controller;

import com.mproduits.configurations.ApplicationPropertiesConfiguration;
import com.mproduits.dao.ProductDao;
import com.mproduits.model.Product;
import com.mproduits.web.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    ProductDao productDao;
    
    @Autowired
    ApplicationPropertiesConfiguration appProperties;
    

    // Affiche la liste de tous les produits disponibles
    @GetMapping(value = "/Produits") 
    public List<Product> listeDesProduits(){

        List<Product> products = productDao.findAll();

        if(products.isEmpty()) throw new ProductNotFoundException("Aucun produit n'est disponible à la vente");

//        System.out.println("limite de produit "+appProperties.getLimiteDeProduits());
//        int maxListe = 1;
//        try {
//        	maxListe = Integer.parseInt(appProperties.getLimiteDeProduits());
//        }catch (NumberFormatException e) {
//        	e.printStackTrace();
//        	return products;
//		}
        return products.subList(0, appProperties.getLimiteDeProduitsNumber());
        
    }

    //Récuperer un produit par son id
    @GetMapping( value = "/Produits/{id}")
    public Optional<Product> recupererUnProduit(@PathVariable int id) {

        Optional<Product> product = productDao.findById(id);

        if(!product.isPresent())  throw new ProductNotFoundException("Le produit correspondant à l'id " + id + " n'existe pas");

        return product;
    }
    
    //TODO creation de produit
    
    public static void main(String[] args) {
    	String s = "4";
    	
    	int i = Integer.parseInt(s);
    	System.out.println("i "+i);
	}
}

