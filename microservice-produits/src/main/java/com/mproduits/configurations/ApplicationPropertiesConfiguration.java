package com.mproduits.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("mes-configs")
@RefreshScope
public class ApplicationPropertiesConfiguration {

	private String limiteDeProduits;

	public String getLimiteDeProduits() {
		return limiteDeProduits;
	}

	public void setLimiteDeProduits(String limiteDeProduits) {
		this.limiteDeProduits = limiteDeProduits;
	}
	
	public int getLimiteDeProduitsNumber() {
		
		 int maxListe = 1;
	        try {
	        	maxListe = Integer.parseInt(limiteDeProduits);
	        }catch (NumberFormatException e) {
	        	e.printStackTrace();	        	
			}
		
		return maxListe;
	}

	
	
	
	
}
